import Data.Char
import Data.List
import Data.Maybe

digits2Num :: [Int] -> Int
digits2Num = foldl1 (\x y -> 10 * x + y)

num2digits :: Int -> [Int]
num2digits = map digitToInt . show

mini :: Int -> Int
mini n = digits2Num [1..n]

maxi :: Int -> Int
maxi n = digits2Num [(10 - n) .. 9]

isAscending' :: [Int] -> Bool
isAscending' ds = and $ zipWith (<) ds (tail ds)

next :: Int -> Int
next n = if isAscending' (num2digits (n + 1)) then (n + 1) else next (n + 1)

valid_readings :: Int -> [Int]
valid_readings n = ([mini n] ++ [(next i) | i  <- [(mini n)..(maxi n)], ((next (i + 1)) /= next i)])

get_index :: [Int] -> Int -> Int
get_index list i = fromJust (findIndex(==i) (list))

next_reading :: Int -> Int -> Int -> Int
next_reading r1 y n = valid_readings n !! (mod (i + y) (length (valid_readings n))) where i = (get_index (valid_readings n) r1)

prev_reading :: Int -> Int -> Int -> Int
prev_reading r1 y n = valid_readings n !! (mod (i - y) (length (valid_readings n))) where i = (get_index (valid_readings n) r1)

diff_bet_readings :: Int -> Int -> Int -> Int
diff_bet_readings r1 r2 n = abs(i - j) 
    where i = (get_index (valid_readings n) r1) 
          j = (get_index (valid_readings n) r2)
--diff_bet_readings r1 r2 n = abs((get_index (valid_readings n) r1) - (get_index (valid_readings n) r2))

main = do
    print $ (valid_readings 2)
    print $ next_reading 29 2 2
    print $ prev_reading 45 3 2
    print $ diff_bet_readings 27 34 2
